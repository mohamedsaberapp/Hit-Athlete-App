import { Component } from '@angular/core';
import { Platform, App, NavControllerBase } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { AuthPage } from '../pages/auth/auth';
import { HomePage } from '../pages/home/home';
import { MySplashPage } from '../pages/my-splash/my-splash';
import { OneSignal } from '@ionic-native/onesignal';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  nav: NavControllerBase;
  rootPage: any = AuthPage;
  
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private app: App, public storage: Storage,private oneSignal:OneSignal) {
    
    platform.ready().then(() => {

      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.oneSignal.startInit('c12e1ebe-d326-43b8-a201-fba16ecfa635', '732956487522');
      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
        console.log("opened");
      });
      
      this.oneSignal.endInit();
    });
  }

}

