import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { OneSignal } from '@ionic-native/onesignal';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  myEmail: any;
  myPlayerId: string;
  loginForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public formBuilder: FormBuilder, public storage: Storage, public events: Events, public toastCtrl: ToastController, private one: OneSignal, private alertCtrl: AlertController) {
    this.one.getIds().then(val => {
      console.log(val);
      // alert('myPlayerId'+val.userId);
      this.myPlayerId = val.userId;
    })
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.required),
    })
  }
  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' }
    ]
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  goLog() {
    this.http.get('http://hitapp.bit68.com/login/?email=' + this.loginForm.value.email + '&password=' + this.loginForm.value.password + '&player_id=' + this.myPlayerId).map(res => res.json()).subscribe(data => {
      console.log(data);
      let toast = this.toastCtrl.create({
        message: 'Successfully logged !',
        duration: 3000
      });
      toast.present();
      this.storage.set('user_info', JSON.stringify(data));
      this.events.publish('user:logged', data);
      this.navCtrl.setRoot(HomePage, { data: data });
    }, err => {
      console.log(err);
      let toast = this.toastCtrl.create({
        message: 'Please fill your information correctly !',
        duration: 3000
      });
      toast.present();

    });
  }
  forgetPassword() {
    let prompt = this.alertCtrl.create({
      title: 'Forgot password ?',
      message: "Enter your Email to reset your password",
      inputs: [
        {
          name: 'Email',
          placeholder: 'Email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            if (data) {
              this.http.get('http://hitapp.bit68.com/forgot_password/?email=' + data.Email).subscribe((res:any) => {
                console.log(res);
                
                let toast = this.toastCtrl.create({
                  message: res._body,
                  duration: 3000
                });
                toast.present();
                
              }, err => {
                console.log(err);
                let toast = this.toastCtrl.create({
                  message: err.statusText,
                  duration: 3000
                });
                toast.present();

              });
              this.myEmail = data.Email;
            }

            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
}
