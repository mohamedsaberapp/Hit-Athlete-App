import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  validations_form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public events: Events, public storage: Storage, public http: Http,public toastCtrl:ToastController) {
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
      password: new FormControl('',
        Validators.compose([
          Validators.minLength(6),
          Validators.required
        ])),
      gender: new FormControl(''),
      date: new FormControl(''),
      phone: new FormControl('',
        Validators.compose([
          Validators.minLength(11),
          Validators.maxLength(11)])),
      weight: new FormControl('',
        Validators.compose([
          Validators.maxLength(3)])),
      height: new FormControl('',
        Validators.compose([
          Validators.maxLength(3)])),
      address: new FormControl(''),
      injury: new FormControl(''),
      referral: new FormControl('')
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'minlength', message: 'Phone must be 11 characters long.' },
      { type: 'maxlength', message: 'Phone must be 11 characters long.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ],
    'gender': [
      { type: 'required', message: 'Gender is required' }
    ],
    'date': [
      { type: 'required', message: 'Date of birth is required' }
    ],
    'weight': [
      { type: 'required', message: 'Weight is required' },
      { type: 'maxlength', message: 'Weight can not be more than 3 numbers long, are you more than 999 kg ?!' }
      
    ],
    'height': [
      { type: 'required', message: 'Height is required' },
      { type: 'maxlength', message: 'Height can not be more than 3 numbers long, are you more than 999 cm ?!' }
    ],
  };
  // goHome() {
  //   this.navCtrl.push(HomePage);
  // }
  onSubmit(values) {
    console.log(values);
    this.http.get('http://hitapp.bit68.com/register/?email=' + values.email + '&password=' + values.password + '&name=' + values.name + '&phone_number=' + values.phone + '&gender=' + values.gender + '&weight=' + values.weight + '&height=' + values.height + '&injury=' + values.injury + '&referral=' + values.referral + '&dob=' + values.date + '&address=' + values.address).map(res => res.json()).subscribe(data => {
      console.log(data);
      let toast = this.toastCtrl.create({
        message: 'Successfully registered!',
        duration: 3000
      });
      toast.present();
      this.storage.set('user_info', JSON.stringify(data));
      this.events.publish('user:logged', data);
      this.navCtrl.setRoot(HomePage, { data: data });
    }, err => {
      console.log(err);
      let toast = this.toastCtrl.create({
        message: err,
        duration: 3000
      });
      toast.present();
    });
  }
}
