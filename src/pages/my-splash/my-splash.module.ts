import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySplashPage } from './my-splash';

@NgModule({
  declarations: [
    MySplashPage,
  ],
  imports: [
    IonicPageModule.forChild(MySplashPage),
  ],
})
export class MySplashPageModule {}
