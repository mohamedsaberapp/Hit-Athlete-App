import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AuthPage } from '../auth/auth';
import { Storage } from '@ionic/storage';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
@IonicPage()
@Component({
  selector: 'page-my-splash',
  templateUrl: 'my-splash.html',
})
export class MySplashPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private app: App, public storage: Storage,private nativePageTransitions:NativePageTransitions ) {
    this.storage.get('user_info').then((val) => {
      setTimeout(() => {
        if (val) {
          this.app.getActiveNav().setRoot(HomePage, { data: JSON.parse(val) });
        } else if (!val) {
          this.app.getActiveNav().setRoot(AuthPage);
        };
       }, 3000);
      
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySplashPage');
  }
  ionViewWillLeave() {
    
     let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
       };
    
     this.nativePageTransitions.slide(options)
       .then(

       )
       .catch(
         
       );
    
    }

}
