import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage) {
    this.storage.get('user_info').then((val) => {
      if(val){
        this.navCtrl.setRoot(HomePage,{data:JSON.parse(val)});
      }
      
    })
  }

  ionViewDidLoad() {
    
  }
  goLog() {
    this.navCtrl.push(LoginPage);

  }
  goReg() {
    this.navCtrl.push(RegisterPage);
  }
  goHome(){
    this.navCtrl.push(HomePage);
  }
}
