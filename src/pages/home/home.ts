import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { AuthPage } from '../auth/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  classe: any;
  programs: any[];
  myUpdatedInfo: any={};
  
  myToken: string;
  myInfo: any;
  text: string;
  choice: string = "Available";
  createdCode=null;

  constructor(public navCtrl: NavController,public navParams:NavParams,public http:Http,public storage:Storage) {
    this.myUpdatedInfo={packages:[],unique_classes:[],admissions:[]};
    // this.programs=[
    //   {name:"Triple threat"},
    //   {name:"Weight Loss and Toning"},
    //   {name:"Focus"},
    //   {name:"Yoga"},
    //   {name:"Jiujitsu"},
    //   {name:"Fast track"},
    //   {name:"Climb"},
    //   {name:"Nutrition Plan (Standard)"},
    //   {name:"Nutrition Coaching"},
    //   {name:"Open Gym"},
    //   {name:"Hit Connect"}
    // ]

  }
  ionViewDidEnter() {
    this.myInfo=this.navParams.get('data');
    this.createdCode=this.myInfo.qrcode;
    this.myToken=this.myInfo.token;
    this.http.get('http://hitapp.bit68.com/user_info/?token=' + this.myToken).map(res => res.json()).subscribe(data => {
      console.log(data)
      this.myUpdatedInfo=data;
      // this.classe=data.unique_classes 
    }, err => {
      console.log(err)
    });
  }
  doRefresh(e){
    console.log(e);
    this.http.get('http://hitapp.bit68.com/user_info/?token=' + this.myToken).map(res => res.json()).subscribe(data => {
      console.log(data)
      this.myUpdatedInfo=data;
      // this.classe=data.unique_classes 
      e.complete();  
    }, err => {
      console.log(err)
    });
  }
  logOut(){
    this.storage.clear();
    this.navCtrl.setRoot(AuthPage);
  }
}
