webpackJsonp([6],{

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular_components_toast_toast_controller__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, http, formBuilder, storage, events, toastCtrl, one, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.events = events;
        this.toastCtrl = toastCtrl;
        this.one = one;
        this.alertCtrl = alertCtrl;
        this.validation_messages = {
            'email': [
                { type: 'required', message: 'Email is required.' },
                { type: 'pattern', message: 'Enter a valid email.' }
            ],
            'password': [
                { type: 'required', message: 'Password is required.' }
            ]
        };
        this.one.getIds().then(function (val) {
            console.log(val);
            // alert('myPlayerId'+val.userId);
            _this.myPlayerId = val.userId;
        });
        this.loginForm = this.formBuilder.group({
            email: new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].compose([
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.goLog = function () {
        var _this = this;
        this.http.get('http://hitapp.bit68.com/login/?email=' + this.loginForm.value.email + '&password=' + this.loginForm.value.password + '&player_id=' + this.myPlayerId).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            var toast = _this.toastCtrl.create({
                message: 'Successfully logged !',
                duration: 3000
            });
            toast.present();
            _this.storage.set('user_info', JSON.stringify(data));
            _this.events.publish('user:logged', data);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { data: data });
        }, function (err) {
            console.log(err);
            var toast = _this.toastCtrl.create({
                message: 'Please fill your information correctly !',
                duration: 3000
            });
            toast.present();
        });
    };
    LoginPage.prototype.forgetPassword = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Forgot password ?',
            message: "Enter your Email to reset your password",
            inputs: [
                {
                    name: 'Email',
                    placeholder: 'Email'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        if (data) {
                            _this.http.get('http://hitapp.bit68.com/forgot_password/?email=' + data.Email).subscribe(function (res) {
                                console.log(res);
                                var toast = _this.toastCtrl.create({
                                    message: res._body,
                                    duration: 3000
                                });
                                toast.present();
                            }, function (err) {
                                console.log(err);
                                var toast = _this.toastCtrl.create({
                                    message: err.statusText,
                                    duration: 3000
                                });
                                toast.present();
                            });
                            _this.myEmail = data.Email;
                        }
                        console.log('Saved clicked');
                    }
                }
            ]
        });
        prompt.present();
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Login</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n    <div text-center>\n        <img src="img/HIT.png">\n    </div>\n    <br>\n    <form [formGroup]="loginForm" (ngSubmit)="goLog(loginForm.values)">\n        <ion-list padding>\n\n            <ion-item>\n                <ion-label stacked>Email</ion-label>\n                <ion-input formControlName="email" type="text"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.email">\n                    <div class="error-message" *ngIf="loginForm.get(\'email\').hasError(validation.type) && (loginForm.get(\'email\').dirty || loginForm.get(\'email\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div>\n\n            <ion-item>\n                <ion-label stacked>Password</ion-label>\n                <ion-input formControlName="password" type="password"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.password">\n                    <div class="error-message" *ngIf="loginForm.get(\'password\').hasError(validation.type) && (loginForm.get(\'password\').dirty || loginForm.get(\'password\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div>\n            <ion-item>\n                <ion-label>Remember me</ion-label>\n                <ion-checkbox checked></ion-checkbox>\n            </ion-item>\n\n        </ion-list>\n        <div padding>\n            <p (click)="forgetPassword()" text-center ion-text color="light">Forgot password ?</p>\n        </div>\n\n        <div padding>\n            <button ion-button outline block color="light" [disabled]="!loginForm.valid">Submit</button>\n        </div>\n    </form>\n    <div class="overlay"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_7_ionic_angular_components_toast_toast_controller__["a" /* ToastController */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_onesignal__["a" /* OneSignal */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular_components_toast_toast_controller__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, formBuilder, events, storage, http, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.events = events;
        this.storage = storage;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.validation_messages = {
            'name': [
                { type: 'required', message: 'Name is required.' }
            ],
            'email': [
                { type: 'required', message: 'Email is required.' },
                { type: 'pattern', message: 'Enter a valid email.' }
            ],
            'phone': [
                { type: 'required', message: 'Phone is required.' },
                { type: 'minlength', message: 'Phone must be 11 characters long.' },
                { type: 'maxlength', message: 'Phone must be 11 characters long.' }
            ],
            'password': [
                { type: 'required', message: 'Password is required.' },
                { type: 'minlength', message: 'Password must be at least 6 characters long.' }
            ],
            'gender': [
                { type: 'required', message: 'Gender is required' }
            ],
            'date': [
                { type: 'required', message: 'Date of birth is required' }
            ],
            'weight': [
                { type: 'required', message: 'Weight is required' },
                { type: 'maxlength', message: 'Weight can not be more than 3 numbers long, are you more than 999 kg ?!' }
            ],
            'height': [
                { type: 'required', message: 'Height is required' },
                { type: 'maxlength', message: 'Height can not be more than 3 numbers long, are you more than 999 cm ?!' }
            ],
        };
        this.validations_form = this.formBuilder.group({
            name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            email: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].minLength(6),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required
            ])),
            gender: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](''),
            date: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](''),
            phone: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].minLength(11),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(11)
            ])),
            weight: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(3)
            ])),
            height: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(3)
            ])),
            address: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](''),
            injury: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](''),
            referral: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]('')
        });
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    // goHome() {
    //   this.navCtrl.push(HomePage);
    // }
    RegisterPage.prototype.onSubmit = function (values) {
        var _this = this;
        console.log(values);
        this.http.get('http://hitapp.bit68.com/register/?email=' + values.email + '&password=' + values.password + '&name=' + values.name + '&phone_number=' + values.phone + '&gender=' + values.gender + '&weight=' + values.weight + '&height=' + values.height + '&injury=' + values.injury + '&referral=' + values.referral + '&dob=' + values.date + '&address=' + values.address).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            var toast = _this.toastCtrl.create({
                message: 'Successfully registered!',
                duration: 3000
            });
            toast.present();
            _this.storage.set('user_info', JSON.stringify(data));
            _this.events.publish('user:logged', data);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { data: data });
        }, function (err) {
            console.log(err);
            var toast = _this.toastCtrl.create({
                message: err,
                duration: 3000
            });
            toast.present();
        });
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-register',template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/pages/register/register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Register</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div text-center>\n        <img src="img/HIT.png">\n    </div>\n    <br>\n    <form padding [formGroup]="validations_form" (ngSubmit)="onSubmit(validations_form.value)">\n        <ion-list>\n\n            <ion-item>\n                <ion-label stacked>* Name</ion-label>\n                <ion-input formControlName="name" type="text"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.name">\n                    <div class="error-message" *ngIf="validations_form.get(\'name\').hasError(validation.type) && (validations_form.get(\'name\').dirty || validations_form.get(\'name\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div>\n\n            <ion-item>\n                <ion-label stacked>* Email</ion-label>\n                <ion-input formControlName="email" type="text"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.email">\n                    <div class="error-message" *ngIf="validations_form.get(\'email\').hasError(validation.type) && (validations_form.get(\'email\').dirty || validations_form.get(\'email\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div>\n\n            <ion-item>\n                <ion-label stacked>Gender</ion-label>\n                <ion-select formControlName="gender">\n                    <ion-option value="male">Male</ion-option>\n                    <ion-option value="female">Female</ion-option>\n                </ion-select>\n            </ion-item>\n            <!-- <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.gender">\n                    <div class="error-message" *ngIf="validations_form.get(\'gender\').hasError(validation.type) && (validations_form.get(\'gender\').dirty || validations_form.get(\'gender\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div> -->\n\n            <ion-item>\n                <ion-label stacked>Date of birth</ion-label>\n                <ion-datetime formControlName="date" ion-text color="secondary"></ion-datetime>\n            </ion-item>\n            <!-- <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.date">\n                    <div class="error-message" *ngIf="validations_form.get(\'date\').hasError(validation.type) && (validations_form.get(\'date\').dirty || validations_form.get(\'date\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div> -->\n\n            <ion-item>\n                <ion-label stacked>Phone Number</ion-label>\n                <ion-input formControlName="phone" type="number"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.phone">\n                    <div class="error-message" *ngIf="validations_form.get(\'phone\').hasError(validation.type) && (validations_form.get(\'phone\').dirty || validations_form.get(\'phone\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div>\n            <ion-item>\n                <ion-label stacked>* Password</ion-label>\n                <ion-input formControlName="password" type="password"></ion-input>\n            </ion-item>\n            <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.password">\n                    <div class="error-message" *ngIf="validations_form.get(\'password\').hasError(validation.type) && (validations_form.get(\'password\').dirty || validations_form.get(\'password\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div>\n\n            <ion-item>\n                <ion-label stacked>Weight</ion-label>\n                <ion-input formControlName="weight" type="number"></ion-input>\n            </ion-item>\n            <!-- <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.weight">\n                    <div class="error-message" *ngIf="validations_form.get(\'weight\').hasError(validation.type) && (validations_form.get(\'weight\').dirty || validations_form.get(\'weight\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div> -->\n\n            <ion-item>\n                <ion-label stacked>Height</ion-label>\n                <ion-input formControlName="height" type="number"></ion-input>\n            </ion-item>\n            <!-- <div class="validation-errors">\n                <ng-container *ngFor="let validation of validation_messages.height">\n                    <div class="error-message" *ngIf="validations_form.get(\'height\').hasError(validation.type) && (validations_form.get(\'height\').dirty || validations_form.get(\'height\').touched)">\n                        {{ validation.message }}\n                    </div>\n                </ng-container>\n            </div> -->\n\n            <ion-item>\n                <ion-label stacked>Home Address</ion-label>\n                <ion-input formControlName="address" type="text"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked>How did you know about us ?</ion-label>\n                <ion-textarea rows="4" formControlName="referral" placeholder="Write briefly"></ion-textarea>\n            </ion-item>\n            <br>\n\n            <ion-item>\n                <ion-label stacked>State any injury</ion-label>\n                <ion-textarea rows="4" formControlName="injury" placeholder="Write briefly"></ion-textarea>\n            </ion-item>\n            <br>\n            <p *ngIf="!validations_form.valid" ion-text color="danger" padding text-center>Please make sure you have filled in all required fields! </p>\n\n        </ion-list>\n        <div padding>\n            <button ion-button outline block color="light" [disabled]="!validations_form.valid">Submit</button>\n        </div>\n    </form>\n    <div class="overlay"></div>\n</ion-content>\n<ion-footer>\n\n</ion-footer>\n'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/pages/register/register.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_7_ionic_angular_components_toast_toast_controller__["a" /* ToastController */]])
], RegisterPage);

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 122:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 122;

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/auth/auth.module": [
		305,
		5
	],
	"../pages/login/login.module": [
		306,
		4
	],
	"../pages/my-splash/my-splash.module": [
		307,
		3
	],
	"../pages/notifications/notifications.module": [
		308,
		0
	],
	"../pages/register/register.module": [
		309,
		2
	],
	"../pages/result/result.module": [
		310,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 164;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MySplashPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_page_transitions__ = __webpack_require__(167);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MySplashPage = (function () {
    function MySplashPage(navCtrl, navParams, app, storage, nativePageTransitions) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.app = app;
        this.storage = storage;
        this.nativePageTransitions = nativePageTransitions;
        this.storage.get('user_info').then(function (val) {
            setTimeout(function () {
                if (val) {
                    _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { data: JSON.parse(val) });
                }
                else if (!val) {
                    _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthPage */]);
                }
                ;
            }, 3000);
        });
    }
    MySplashPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MySplashPage');
    };
    MySplashPage.prototype.ionViewWillLeave = function () {
        var options = {
            direction: 'up',
            duration: 500,
            slowdownfactor: 3,
            slidePixels: 20,
            iosdelay: 100,
            androiddelay: 150,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 60
        };
        this.nativePageTransitions.slide(options)
            .then()
            .catch();
    };
    return MySplashPage;
}());
MySplashPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-my-splash',template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/pages/my-splash/my-splash.html"*/'\n\n\n<ion-content>\n<!-- <img src="img/mySplash.png"> -->\n</ion-content>\n'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/pages/my-splash/my-splash.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_page_transitions__["a" /* NativePageTransitions */]])
], MySplashPage);

//# sourceMappingURL=my-splash.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResultPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ResultPage = (function () {
    function ResultPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.choice = 'Available';
    }
    ResultPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResultPage');
    };
    return ResultPage;
}());
ResultPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-result',template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/pages/result/result.html"*/'<!--\n  Generated template for the ResultPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color="dark">\n        <ion-title>QR Results</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/pages/result/result.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
], ResultPage);

//# sourceMappingURL=result.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(232);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_qr_scanner__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_qrcode2__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(304);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_register_register__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_auth_auth__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_result_result__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_my_splash_my_splash__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_native_page_transitions__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_onesignal__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_auth_auth__["a" /* AuthPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_result_result__["a" /* ResultPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_my_splash_my_splash__["a" /* MySplashPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_qrcode2__["a" /* NgxQRCodeModule */],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/auth/auth.module#AuthPageModule', name: 'AuthPage', segment: 'auth', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/my-splash/my-splash.module#MySplashPageModule', name: 'MySplashPage', segment: 'my-splash', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/notifications/notifications.module#NotificationsPageModule', name: 'NotificationsPage', segment: 'notifications', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/result/result.module#ResultPageModule', name: 'ResultPage', segment: 'result', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["a" /* IonicStorageModule */].forRoot()
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_auth_auth__["a" /* AuthPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_result_result__["a" /* ResultPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_my_splash_my_splash__["a" /* MySplashPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_qr_scanner__["a" /* QRScanner */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_onesignal__["a" /* OneSignal */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 304:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_auth_auth__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__ = __webpack_require__(89);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, app, storage, oneSignal) {
        var _this = this;
        this.app = app;
        this.storage = storage;
        this.oneSignal = oneSignal;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_auth_auth__["a" /* AuthPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            _this.oneSignal.startInit('c12e1ebe-d326-43b8-a201-fba16ecfa635', '732956487522');
            _this.oneSignal.handleNotificationOpened().subscribe(function () {
                // do something when a notification is opened
                console.log("opened");
            });
            _this.oneSignal.endInit();
        });
    }
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_onesignal__["a" /* OneSignal */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = (function () {
    function HomePage(navCtrl, navParams, http, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.myUpdatedInfo = {};
        this.choice = "Available";
        this.createdCode = null;
        this.myUpdatedInfo = { packages: [], unique_classes: [], admissions: [] };
        // this.programs=[
        //   {name:"Triple threat"},
        //   {name:"Weight Loss and Toning"},
        //   {name:"Focus"},
        //   {name:"Yoga"},
        //   {name:"Jiujitsu"},
        //   {name:"Fast track"},
        //   {name:"Climb"},
        //   {name:"Nutrition Plan (Standard)"},
        //   {name:"Nutrition Coaching"},
        //   {name:"Open Gym"},
        //   {name:"Hit Connect"}
        // ]
    }
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.myInfo = this.navParams.get('data');
        this.createdCode = this.myInfo.qrcode;
        this.myToken = this.myInfo.token;
        this.http.get('http://hitapp.bit68.com/user_info/?token=' + this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myUpdatedInfo = data;
            // this.classe=data.unique_classes 
        }, function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.doRefresh = function (e) {
        var _this = this;
        console.log(e);
        this.http.get('http://hitapp.bit68.com/user_info/?token=' + this.myToken).map(function (res) { return res.json(); }).subscribe(function (data) {
            console.log(data);
            _this.myUpdatedInfo = data;
            // this.classe=data.unique_classes 
            e.complete();
        }, function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.logOut = function () {
        this.storage.clear();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__auth_auth__["a" /* AuthPage */]);
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/pages/home/home.html"*/'<ion-header>\n    <ion-navbar color="dark">\n        <ion-buttons end>\n            <button ion-button icon-only (click)="logOut()">\n                <ion-icon color="danger" name="md-log-out"></ion-icon>\n            </button>\n\n\n        </ion-buttons>\n        <ion-title>\n            Home\n        </ion-title>\n        <!-- <ion-buttons end>\n            <button item-end ion-button icon-only (click)="openNotifications()">\n                        <ion-icon color="danger"  name="md-notifications"></ion-icon>\n                        <ion-badge style="margin-left:-10px;" color="light">2</ion-badge>\n                    </button>\n        </ion-buttons> -->\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <p padding>Welcome\n        <strong ion-text color="danger">"{{myUpdatedInfo.name}}"</strong>\n    </p>\n    <ion-card *ngIf="createdCode">\n        <ngx-qrcode [qrc-value]="myUpdatedInfo.qrcode"></ngx-qrcode>\n    </ion-card>\n    <div padding>\n        <ion-segment color="danger" [(ngModel)]="choice">\n            <ion-segment-button value="Available">\n                Available\n            </ion-segment-button>\n            <ion-segment-button value="History">\n                History\n            </ion-segment-button>\n            <ion-segment-button value="Programs">\n                Programs\n            </ion-segment-button>\n        </ion-segment>\n    </div>\n\n    <div [ngSwitch]="choice">\n        <ion-list *ngSwitchCase="\'Available\'">\n            <p padding color="gray" ion-text text-center *ngIf="!myUpdatedInfo.packages[0]">\n                <ion-icon class="large" name="ios-alert-outline"></ion-icon>\n                <br>You don\'t have any subscriptions</p>\n            <div *ngFor="let sub of myUpdatedInfo.packages">\n\n                <ion-card padding>\n                    <div class="counter" text-center>\n                        <strong>{{sub.title}}</strong>\n                        <h1 ion-text color="danger">{{sub.remaining_sessions}}</h1>\n                        <p>Remaining Sessions</p>\n                    </div>\n                    <ion-row text-center>\n                        <ion-col *ngFor="let branch of sub.branches">\n                            <button ion-button small outline color="dark">\n                                <strong class="mg_r">{{branch}} </strong> branch</button>\n                        </ion-col>\n                    </ion-row>\n                    <div padding>\n                        <p class="valid" text-center>Valid until :\n                            <strong ion-text color="danger">{{sub.valid_until}}</strong>\n                        </p>\n                    </div>\n                </ion-card>\n            </div>\n        </ion-list>\n        <ion-list *ngSwitchCase="\'History\'">\n            <p padding color="gray" ion-text text-center *ngIf="!myUpdatedInfo.admissions[0]">\n                <ion-icon class="large" name="ios-alert-outline"></ion-icon>\n                <br> You don\'t have any previous sessions</p>\n            <div *ngFor="let sess of myUpdatedInfo.admissions">\n                <ion-item>\n                    <h2>\n                        <ion-icon name="navigate"></ion-icon> {{sess.branch}}</h2>\n                    <strong>\n                        <ion-icon name="bookmark"></ion-icon> {{sess.class}}</strong>\n                    <p>\n                        <ion-icon name="calendar"></ion-icon> {{sess.date}}</p>\n                    <p>\n                        <ion-icon name="clock"></ion-icon> {{sess.time}}</p>\n                </ion-item>\n            </div>\n        </ion-list>\n        <ion-list *ngSwitchCase="\'Programs\'">\n                <p padding color="gray" ion-text text-center *ngIf="!myUpdatedInfo.unique_classes[0]">\n                        <ion-icon class="large" name="ios-alert-outline"></ion-icon>\n                        <br> You don\'t have any previous programs</p>\n            <div *ngFor="let prog of myUpdatedInfo.unique_classes">\n                <ion-card padding>\n                    <h2>{{prog.title}}</h2>\n                    <!-- <strong>{{pack.package_type}}</strong><br><br> -->\n                    <p>{{prog.description}}</p>\n                </ion-card>\n            </div>\n        </ion-list>\n    </div>\n</ion-content>\n<!-- <ion-footer padding>\n    <p (click)="goRes(text)">Scanned Qr : <strong ion-text color="danger">{{text}}</strong></p>\n</ion-footer> -->'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AuthPage = (function () {
    function AuthPage(navCtrl, navParams, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.storage.get('user_info').then(function (val) {
            if (val) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { data: JSON.parse(val) });
            }
        });
    }
    AuthPage.prototype.ionViewDidLoad = function () {
    };
    AuthPage.prototype.goLog = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    AuthPage.prototype.goReg = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    AuthPage.prototype.goHome = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    return AuthPage;
}());
AuthPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-auth',template:/*ion-inline-start:"/Users/mini/Desktop/Gym/src/pages/auth/auth.html"*/'\n<ion-content>\n    \n    <div class="overlay">\n      <div class="background" text-center><img src="img/HIT.png"></div>\n      <div class="controls" padding>\n        <button ion-button outline block color="light" (click)="goLog()">Login</button>\n        <button ion-button outline block color="light" (click)="goReg()">Register</button>\n      </div>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mini/Desktop/Gym/src/pages/auth/auth.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], AuthPage);

//# sourceMappingURL=auth.js.map

/***/ })

},[213]);
//# sourceMappingURL=main.js.map